
import { Component } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup } from "@angular/forms";

@Component({
    selector: 'create',
    templateUrl: './create.template.html',
    styleUrls: ['./create.component.scss']
})

export class CreateComponent  {
    public view = '';
    public data: FormGroup = new FormGroup({});

    constructor(
        private param: ActivatedRoute,
        private location: Location,
    ) {
        this.param.params.subscribe(param => {
            Object.keys( this.data.value ).forEach(key => {this.data.removeControl(key)})
            this.view = param.mdl;
        })
    }    
     
    create() {
        console.log('create data', this.data.value);
    }

    goBack() {
        this.location.back();
    }
}
