import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ProductsComponent } from './products/products.component';
import { ListComponent } from './list/list.component';
import { GridModule, ExcelModule, PDFModule } from '@progress/kendo-angular-grid';
import { WidgetModule } from '../../layout/widget/widget.module';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { UsersComponent } from './users/users.component';


export const routes = [
  { path: 'create/:mdl', component: CreateComponent, pathMatch: 'full' },
  { path: 'edit/:mdl/:id', component: EditComponent, pathMatch: 'full' },
  { path: 'list/:mdl', component: ListComponent, pathMatch: 'full' }
];


@NgModule({
  imports: [
    CommonModule,
    GridModule,
    ExcelModule,
    PDFModule,
    FormsModule,
    ReactiveFormsModule,
    WidgetModule,
    DialogModule,
    MultiSelectModule,
    TableModule,
    RouterModule.forChild(routes)

  ],
  declarations: [
    CreateComponent,
    EditComponent,
    ListComponent,
    ProductsComponent,
    UsersComponent
  ]
})
export class ModulesModule {
  static routes = routes;
}
