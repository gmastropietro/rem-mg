
import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { exampleData, fields } from "./products";

@Component({
    selector: 'products',
    templateUrl: './products.template.html',
    styleUrls: ['../form.component.scss']
})

export class ProductsComponent implements OnInit {
    @Input() id: string;
    @Input() product: FormGroup;
    public floorPlans: FormGroup;
    public officeHours: FormGroup;
    public amenities: FormGroup;
    public creatingFloorPlan: boolean = false;
    public creatingOfficeHour: boolean = false;
    public displayLookup: boolean = false;
    public index: number;
    public lookupFields: any[] = [];
    public lookup_data: any;
    public lookup_selected: any;
    public wichone: string;
    public globalFilterFields: any[] = [];
    public days = [
        { label: 'Monday', value: 'Monday' },
        { label: 'Tuesday', value: 'Tuesday' },
        { label: 'Wednesday', value: 'Wednesday' },
        { label: 'Thursday', value: 'Thursday' },
        { label: 'Friday', value: 'Friday' },
        { label: 'Saturday', value: 'Saturday' },
        { label: 'Sunday', value: 'Sunday' },
    ];

        constructor(private formBuilder: FormBuilder) { }

ngOnInit() {
    this.createForms()
}

createForms() {
    this.product.addControl('name', new FormControl(''))
    this.product.addControl('originId', new FormControl(''))
    this.product.addControl('origin', new FormControl(''))
    this.product.addControl('description', new FormControl(''))
    this.product.addControl('floorPlans', new FormControl([]))
    this.product.addControl('expenses', new FormControl([]))
    this.product.addControl('amenities', new FormControl([]))
    this.product.addControl('languages', new FormControl([]))
    this.product.addControl('officeHours', new FormControl([]))
    this.product.addControl('location', new FormControl(''))
    this.product.addControl('photos', new FormControl([]))

    this.floorPlans = new FormGroup({
        bedrooms: new FormControl(0),
        bathrooms: new FormControl(0),
        name: new FormControl(''),
        unit: new FormControl(''),
        available: new FormControl(''),
        size: new FormControl(0),
        deposit: new FormControl(0),
        price: new FormControl([]),
    })

    this.officeHours = new FormGroup({
        dayOfWeek: new FormControl([]),
        opens: new FormControl(''),
        closes: new FormControl(''),
    })
}

addFloorPlan() {
    let data = [...this.product.get('floorPlans').value, ...this.floorPlans.value];
    this.product.patchValue({ floorPlans: data });
    this.floorPlans.reset();
}

addElemenet(whichOne) {
    let data = [...this.product.get(whichOne).value, ...this[whichOne].value];
    this.product.patchValue({ [whichOne]: data });
    this[whichOne].reset();
}

delete (whichOne, index) {
    let data = this.product.get(whichOne).value;
    data.splice(index, 1);
    this.product.patchValue({ [whichOne]: data });
}

edit(whichOne, index) {
    this[whichOne].patchValue(this.product.get(whichOne).value[index]);
    this.index = index;
}

update(whichOne) {
    let data: any[] = this.product.get(whichOne).value;
    data[this.index] = this[whichOne].value;
    this.product.patchValue({ [whichOne]: data });
    this.index = null;
}

onDisplayLookup(wichone: string){
    this.lookupFields = fields[wichone];
    this.lookup_data = exampleData[wichone];
    this.globalFilterFields = this.lookupFields.map(el => el.name)
    this.lookup_selected = this.product.get(wichone).value;
    this.wichone = wichone;
    this.displayLookup = true;
}

lookup_callback(){
    console.log(this.lookup_selected);

    this.product.patchValue(
        { [this.wichone]: this.lookup_selected }
    )
}
}
