// Generated by https://quicktype.io

export interface Products {
    name:        string;
    originId:    string;
    origin:      string;
    description: string;
    floorPlans:  FloorPlan[];
    amenities:   Amenity[];
    expenses:    Expense[];
    languages:   string[];
    officeHours: OfficeHour[];
    location:    Location;
    photos:      Photo[];
}

export interface Amenity {
    name:     string;
    category: string;
    title:    string;
}

export interface Expense {
    type:  string;
    name:  string;
    price: string;
}

export interface FloorPlan {
    bedrooms:  number;
    bathrooms: number;
    name:      string;
    unit:      string;
    available: string;
    size:      number;
    deposit:   number;
    price:     number[];
}

export interface Location {
    coord:    Coord;
    country:  string;
    locality: string;
    city:     string;
    state:    string;
    zipCode:  string;
}

export interface Coord {
    type:        string;
    coordinates: number[];
}

export interface OfficeHour {
    dayOfWeek: string[];
    opens:     string;
    closes:    string;
}

export interface Photo {
    src:            string;
    thumbnail:      string;
    description?:   string;
    mediaType:      number;
    attachmentType: number;
    altText:        string;
}
