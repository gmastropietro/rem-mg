
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {
    GridDataResult,
    DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { GridComponent } from '@progress/kendo-angular-grid';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { process, State } from '@progress/kendo-data-query';


@Component({
    selector: 'list',
    templateUrl: './list.template.html',
    styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
    public module = '';
    public gridData: GridDataResult;
    public state: State = {
        skip: 0,
        take: 20
    };
    public data = [
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Dos", amount: 3541, type: "House" },
        { name: "Tres", amount: 2540, type: "House" },
        { name: "Cuatro", amount: 5000, type: "House" },
        { name: "Cinco", amount: 850, type: "Apartment" },
        { name: "Seis", amount: 900, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Dos", amount: 3541, type: "House" },
        { name: "Tres", amount: 2540, type: "House" },
        { name: "Cuatro", amount: 5000, type: "House" },
        { name: "Cuatro", amount: 5000, type: "House" },
        { name: "Cinco", amount: 850, type: "Apartment" },
        { name: "Seis", amount: 900, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Dos", amount: 3541, type: "House" },
        { name: "Tres", amount: 2540, type: "House" },
        { name: "Cuatro", amount: 5000, type: "House" },
        { name: "Cinco", amount: 850, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Dos", amount: 3541, type: "House" },
        { name: "Tres", amount: 2540, type: "House" },
        { name: "Cuatro", amount: 5000, type: "House" },
        { name: "Cuatro", amount: 5000, type: "House" },
        { name: "Cinco", amount: 850, type: "Apartment" },
        { name: "Seis", amount: 900, type: "Apartment" },
        { name: "Uno", amount: 1000, type: "Apartment" },
        { name: "Dos", amount: 3541, type: "House" },
        { name: "Tres", amount: 2540, type: "House" },
        { name: "Cuatro", amount: 5000, type: "House" },
        { name: "Cinco", amount: 850, type: "Apartment" },
        { name: "Seis", amount: 900, type: "Apartment" },
        { name: "Siete", amount: 750, type: "Apartment" }
    ];
    public config = [
        { "label": "Name", "name": "name", "type": "", "width": "" },
        { "label": "Amount", "name": "amount", "type": "number", "width": "" },
        { "label": "Type", "name": "type", "type": "", "width": "" },
    ]


    constructor(
        private param: ActivatedRoute,
        private location: Location,
    ) {
        this.param.params.subscribe(param => {
            this.module = param.mdl;
        })
    }

    ngOnInit() {
        this.gridData = process(this.data, this.state);
    }

    dataStateChange(state: DataStateChangeEvent) {
        this.state = state;
        this.gridData = process(this.data, this.state);
    }

    edit() {
        console.log('editaste');
    }

    goBack() {
        this.location.back();
    }
}
