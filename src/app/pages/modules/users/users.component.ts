
import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
@Component({
    selector: 'users',
    templateUrl: './users.template.html',
    styleUrls: ['../form.component.scss']
})

export class UsersComponent implements OnInit {
    @Input() id: string;
    @Input() user: FormGroup;
    

    constructor(private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.createForms()
    }

    createForms() {
        this.user.addControl('name', new FormControl('',Validators.required));
        this.user.addControl('lastName', new FormControl('',));
        this.user.addControl('email', new FormControl('',[Validators.required,Validators.email]));
        this.user.addControl('password', new FormControl('',[Validators.required,Validators.minLength(3)]));
    }

    
}
