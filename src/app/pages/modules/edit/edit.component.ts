
import { Component } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup } from "@angular/forms";

@Component({
    selector: 'edit',
    templateUrl: './edit.template.html',
    styleUrls: ['./edit.component.scss']
})

export class EditComponent {
    public view = '';
    public id = '';
    public data: FormGroup = new FormGroup({});
    public params:any;

    constructor(
        private param: ActivatedRoute,
        private location: Location,
    ) {
        this.param.params.subscribe(param => {
            Object.keys( this.data.value ).forEach(key => {this.data.removeControl(key)})
            this.view = param.mdl;
            this.id = param.id;
        })
    }

    edit() {
        console.log('editaste');
    }

    goBack() {
        this.location.back();
    }
}
